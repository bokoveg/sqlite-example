#include <sqlite3.h>
#include "gtest/gtest.h"
#include <string>
#include <memory>
#include <string.h>
#include <vector>
#include <string>


namespace sqlite {
    void HandleStatusCode(int status_code, std::string on_error_msg) {
        if (status_code == SQLITE_OK) {
            return;
        } else {
            throw std::runtime_error(on_error_msg + "\nSqlite3 status code: " + 
                                     std::to_string(status_code));
        }
    }

    class Transaction;
    class Statement;

    class Database {
    public:
        Database(const std::string& filename);
        void Execute(const std::string& query);
        std::unique_ptr<Statement> PrepareStatement(const std::string& query);
        std::unique_ptr<Transaction> BeginTransaction();
        ~Database() {
            sqlite3_close(db_);
        }
    private:
        sqlite3 *db_;
    };

    class Transaction {
    public:
        Transaction(Database* db);
        ~Transaction();
        void Commit();
        void Abort();
    private:
        Database *db_;
    };

    class Row {
    public:
        Row(sqlite3_stmt *statement);
        int GetColumnsCount();
        int GetInt(int i);
        std::string GetString(int i);

    private:
        std::vector<std::string> data_;
    };

    class RowSet {
    public:
        using RowIterator = std::vector <Row>::iterator;

        int GetRowNumber();
        RowSet(std::vector<Row> &rows) : rows_(rows) {}
        RowIterator begin();
        RowIterator end();

    private:
        std::vector<Row> rows_;
    };

    class Statement {
    public:
        Statement(sqlite3_stmt* statement);
        void Bind(int i, int param);
        void Bind(int i, const std::string& param);
        void Reset();
        std::unique_ptr<RowSet> Execute();
    private:
        sqlite3_stmt* statement_;
    };


    Database::Database(const std::string& filename) {
        int result_code = sqlite3_open(filename.c_str(), &db_);
        if (result_code == SQLITE_OK) {
            HandleStatusCode(result_code, "Failed to open Database");
        }
    };

    void Database::Execute(const std::string& query) {
        char *errmsg;
        int result_code = sqlite3_exec(db_, query.c_str(), nullptr, nullptr, &errmsg);
        if (result_code != SQLITE_OK) {
            HandleStatusCode(result_code, "Failed to open Database");
        }
    }

    std::unique_ptr<Statement> Database::PrepareStatement(const std::string& query) {
        sqlite3_stmt *statement;
        int result_code = sqlite3_prepare(db_, query.c_str(), -1, &statement, nullptr);
        HandleStatusCode(result_code, std::string("Failed to create query"));
        return std::unique_ptr<Statement>(new Statement(statement));
    };

    std::unique_ptr<Transaction> Database::BeginTransaction() {
        return std::unique_ptr<Transaction>(new Transaction(this));
    };

    Row::Row(sqlite3_stmt *statement) {
        for (size_t i = 0; i < sqlite3_column_count(statement); ++i) {
            auto element = sqlite3_column_text(statement, i);
            data_.push_back(std::string(reinterpret_cast<const char*>(element)));
        }
    }

    int Row::GetColumnsCount() {
        return data_.size();
    };

    int Row::GetInt(int i) {
        if (i < data_.size()) {
            return atoi(data_[i].c_str());
        } else {
            throw std::runtime_error("Column index out of range");
        }
    };

    std::string Row::GetString(int i) {
        if (i < data_.size()) {
            return data_[i].c_str();
        } else {
            throw std::runtime_error("Column index out of range");
        }
    };

    RowSet::RowIterator RowSet::begin() {
        return rows_.begin();
    };
    RowSet::RowIterator RowSet::end() {
        return rows_.end();
    };
    int RowSet::GetRowNumber() {
        return rows_.size();
    }

    Statement::Statement(sqlite3_stmt* statement) {
        statement_ = statement;
    };
    void Statement::Reset() {
        sqlite3_reset(statement_);
    }
    void Statement::Bind(int i, int param) {
        int result_code = sqlite3_bind_int(statement_, i, param);
        HandleStatusCode(result_code, std::string("Failed to bind param"));
    };
    void Statement::Bind(int i, const std::string& param) {
        int result_code = sqlite3_bind_text(statement_,
            i, param.c_str(), -1, nullptr);
        HandleStatusCode(result_code, std::string("Failed to bind param"));
    };

    std::unique_ptr<RowSet> Statement::Execute() {
        std::vector<Row> rows;
        Reset();
        while (true) {
            int result_code = sqlite3_step(statement_);
            if (result_code == SQLITE_ROW) {
                rows.push_back(Row(statement_));
            }
            if (result_code == SQLITE_DONE) {
                return std::unique_ptr<RowSet>(new RowSet(rows));
            }
            if (result_code != SQLITE_ROW && result_code != SQLITE_DONE) {
                HandleStatusCode(result_code, std::string("Failed to execute statement:"
                    + std::to_string(result_code)));
            }
        }
    };


    Transaction::Transaction(Database* db) {
        db_ = db;
        db_->Execute("BEGIN TRANSACTION;");
    }

    Transaction::~Transaction() {
        if (db_ != nullptr) {
            db_->Execute("ROLLBACK;");
        }
    };

    void Transaction::Commit() {
        if (db_ != nullptr) {
            db_->Execute("COMMIT;");
            db_ = nullptr;
        } else {
            throw std::runtime_error("Failed to commit transaction: transaction was closed.");
        }
    };
    void Transaction::Abort() {
        if (db_ != nullptr) {
            db_->Execute("ROLLBACK;");
            db_ = nullptr;
        } else {
            throw std::runtime_error("Failed to commit transaction: transaction was closed.");
        }
    }
}

std::unique_ptr<sqlite::Database> CreateTestDatabase() {
    std::string q_create = "create table test_table("
        "id integer primary key,"
        "value blob"
        ");";
    std::unique_ptr<sqlite::Database> db =
                                std::unique_ptr<sqlite::Database>(new sqlite::Database(""));
    db->Execute(q_create);
    std::string q_fill = "INSERT INTO test_table VALUES (1, 'abc');"
        "INSERT INTO test_table VALUES (3, 'bcd');"
        "INSERT INTO test_table VALUES (5, 'def');"
        "INSERT INTO test_table VALUES (7, 'xyzw');";
    db->Execute(q_fill);
    return db;
}

TEST(sqlite, simple_creation) {
    sqlite::Database db = sqlite::Database("");
}

TEST(sqlite, simple_test_select) {
    auto db = CreateTestDatabase();
    std::string q_select = "SELECT value FROM test_table WHERE id=1";

    auto statement = db->PrepareStatement(q_select);
    auto result = statement->Execute();
    ASSERT_EQ(result->begin()->GetString(0), "abc");
}

TEST(bind, simple_test_bind) {
    auto db = CreateTestDatabase();
    std::string q_select = "SELECT value FROM test_table WHERE id=?";
    for (size_t i = 0; i < 8; ++i) {
        auto statement = db->PrepareStatement(q_select);
        statement->Bind(1, i);
        auto result = statement->Execute();
        ASSERT_EQ(result->GetRowNumber(), i % 2);
    }
}

TEST(transaction, transaction_abort) {
    auto db = CreateTestDatabase();
    std::string q_select = "SELECT value FROM test_table WHERE id=2";
    std::string q_rollback_insert = "INSERT INTO test_table VALUES (2, 'xyz')";

    auto transaction = db->BeginTransaction();
    db->Execute(q_rollback_insert);
    transaction->Abort();

    auto statement = db->PrepareStatement(q_select);
    auto result = statement->Execute();
    ASSERT_EQ(result->GetRowNumber(), 0);
}

TEST(transaction, transaction_destroy) {
    auto db = CreateTestDatabase();
    std::string q_select = "SELECT value FROM test_table WHERE id=2";
    std::string q_destroy_insert = "INSERT INTO test_table VALUES (2, 'xyz')";

    {
        auto local_transaction = db->BeginTransaction();
        db->Execute(q_destroy_insert);
    }

    auto statement = db->PrepareStatement(q_select);
    auto result = statement->Execute();
    ASSERT_EQ(result->GetRowNumber(), 0);
}

TEST(transaction, transaction_commit) {
    auto db = CreateTestDatabase();
    std::string q_select = "SELECT value FROM test_table WHERE id=2";
    std::string q_commit_insert = "INSERT INTO test_table VALUES (2, 'xyz')";

    auto transaction = db->BeginTransaction();
    db->Execute(q_commit_insert);
    transaction->Commit();

    auto statement = db->PrepareStatement(q_select);
    auto result = statement->Execute();
    ASSERT_EQ(result->GetRowNumber(), 1);
}

TEST(sqlite, incorrect_query) {
    auto db = CreateTestDatabase();
    std::string incorrect = "SELECT valu FROM test_table WHERE id=2";

    try {
        auto statement = db->PrepareStatement(incorrect);
        ASSERT_TRUE(false);
    }
    catch (std::runtime_error e) {
        ASSERT_TRUE(true);
    }
}

TEST(bind, bind_out_of_range) {
    auto db = CreateTestDatabase();
    std::string q_select = "SELECT value FROM test_table WHERE id=?";
    
    auto statement = db->PrepareStatement(q_select);
    try {
        statement->Bind(2, 3);
        ASSERT_TRUE(false);
    }
    catch (std::runtime_error e) {
        ASSERT_TRUE(true);
    }
}

TEST(row, col_number_out_of_range) {
    auto db = CreateTestDatabase();
    std::string q_select = "SELECT value FROM test_table WHERE id=1";

    auto statement = db->PrepareStatement(q_select);
    auto result = statement->Execute();
    std::cout << result->GetRowNumber();
    try {
        std::string value = result->begin()->GetString(100);
        ASSERT_TRUE(false);
    }
    catch (std::runtime_error) {
        ASSERT_TRUE(true);
    }
}

TEST(transaction, transaction_must_be_closed) {
    auto db = CreateTestDatabase();
    std::string q_select = "SELECT value FROM test_table WHERE id=2";
    std::string q_commit_insert = "INSERT INTO test_table VALUES (2, 'xyz')";

    auto transaction = db->BeginTransaction();
    db->Execute(q_commit_insert);
    transaction->Commit();
    try {
        transaction->Abort();
        ASSERT_TRUE(false);
    }
    catch (std::runtime_error) {
        ASSERT_TRUE(true);
    }

    try {
        transaction->Commit();
        ASSERT_TRUE(false);
    }
    catch (std::runtime_error) {
        ASSERT_TRUE(true);
    }
};

TEST(transaction, transaction_parts) {
    auto db = CreateTestDatabase();
    std::string q_select = "SELECT * FROM test_table WHERE value='xyz'";
    std::string q_insert_first = "INSERT INTO test_table VALUES (2, 'xyz')";
    std::string q_insert_second = "INSERT INTO test_table VALUES (4, 'xyz')";

    auto transaction = db->BeginTransaction();
    db->Execute(q_insert_first);

    auto statement = db->PrepareStatement(q_select);
    auto result = statement->Execute();
    ASSERT_EQ(result->GetRowNumber(), 1);

    db->Execute(q_insert_second);

    result = statement->Execute();
    ASSERT_EQ(result->GetRowNumber(), 2);

    transaction->Commit();

    statement = db->PrepareStatement(q_select);
    result = statement->Execute();
    ASSERT_EQ(result->GetRowNumber(), 2);
}

TEST(bind, rebind) {
    auto db = CreateTestDatabase();
    std::string q_select = "SELECT value FROM test_table WHERE id=?";

    auto statement = db->PrepareStatement(q_select);
    statement->Bind(1, 3);
    auto result = statement->Execute();

    ASSERT_EQ(result->begin()->GetString(0), "bcd");
    
    statement->Reset();
    statement->Bind(1, 5);
    result = statement->Execute();
    ASSERT_EQ(result->begin()->GetString(0), "def");
}
